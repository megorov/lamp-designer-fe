import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {straightFilter, curvedFilter} from '../redux/actions';

const useStyles = makeStyles((theme) => ({
  filterButtons: {
    marginTop: theme.spacing(4),
  }
}));

const getVariant = (flag) => (flag?"contained": "outlined");
const mapDispatchToProps = {straightFilter, curvedFilter}

function FilterGroup(props) {
    const classes = useStyles()
    const [straight, setStraight] = useState(true)
    const [curved, setCurved] = useState(true)

    const straightFilterOnClick = () => {
        const newValue = !straight
        props.straightFilter(newValue)
        setStraight(newValue)
    }

    const curvedFilterOnClick = () => {
        const newValue = !curved
        props.curvedFilter(newValue)
        setCurved(newValue)
    }

      return (
        <React.Fragment>
                <div className={classes.filterButtons}>
                  <Grid container spacing={2} justify="center">
                    <Grid item>
                      <Button variant={getVariant(straight)} color="primary" onClick={straightFilterOnClick}>
                        straight
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button variant={getVariant(curved)} color="primary" onClick={curvedFilterOnClick}>
                        curved
                      </Button>
                    </Grid>
                  </Grid>
                </div>
        </React.Fragment>
      );
}
export default connect(null, mapDispatchToProps)(FilterGroup)