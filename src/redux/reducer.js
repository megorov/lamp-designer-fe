const initialState = {
    straightFilterOn: true,
    curvedFilterOn: true
    };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'filter/straight':
      return {
        ...state,
        straightFilterOn: action.value
      }
    case 'filter/curved':
          return {
            ...state,
            curvedFilterOn: action.value
          }
    default:
      return state
  }
};

export default reducer;