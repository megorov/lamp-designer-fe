export const straightFilter = isOn => ({type: 'filter/straight', value: isOn})
export const curvedFilter = isOn => ({type: 'filter/curved', value: isOn})